package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.IAuthRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotLoggedInException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedNotAuthorizedException;
import ru.tsc.ichaplygina.taskmanager.model.User;

import static ru.tsc.ichaplygina.taskmanager.util.HashUtil.salt;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public class AuthService implements IAuthService {

    private final IAuthRepository authRepository;

    private final IUserService userService;

    public AuthService(final IAuthRepository authRepository, final IUserService userService) {
        this.authRepository = authRepository;
        this.userService = userService;
    }

    @Override
    public String getCurrentUserId() {
        return authRepository.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(final String currentUserId) {
        authRepository.setCurrentUserId(currentUserId);
    }

    @Override
    public String getCurrentUserLogin() throws AbstractException {
        throwExceptionIfNotAuthorized();
        return userService.findById(getCurrentUserId()).getLogin();
    }

    @Override
    public boolean isNoUserLoggedIn() {
        return isEmptyString(authRepository.getCurrentUserId());
    }

    @Override
    public boolean isPrivilegedUser() throws AbstractException {
        throwExceptionIfNotAuthorized();
        return userService.findById(getCurrentUserId()).getRole().equals(Role.ADMIN);
    }

    @Override
    public void login(final String login, final String password) throws AbstractException {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectCredentialsException();
        if (!salt(password).equals(user.getPasswordHash())) throw new IncorrectCredentialsException();
        setCurrentUserId(user.getId());
    }

    @Override
    public void logout() throws AbstractException {
        if (isNoUserLoggedIn()) throw new UserNotLoggedInException();
        setCurrentUserId(null);
    }

    @Override
    public void throwExceptionIfNotAuthorized() throws AbstractException {
        if (isNoUserLoggedIn()) throw new AccessDeniedNotAuthorizedException();
    }

    @Override
    public void throwExceptionIfNotPrivilegedUser() throws AbstractException {
        if (!isPrivilegedUser()) throw new AccessDeniedException();
    }

}
