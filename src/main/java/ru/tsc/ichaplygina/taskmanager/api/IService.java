package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import java.util.List;

public interface IService<E extends AbstractModel> {

    void add(E entity);

    List<E> findAll() throws AbstractException;

    E findById(String id) throws AbstractException;

    int getSize() throws AbstractException;

    boolean isEmpty() throws AbstractException;

    E removeById(String id) throws AbstractException;
}
