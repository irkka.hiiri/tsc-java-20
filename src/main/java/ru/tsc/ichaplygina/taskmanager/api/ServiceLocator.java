package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.api.service.*;

public interface ServiceLocator {

    IAuthService getAuthService();

    ICommandService getCommandService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

    IUserService getUserService();
}
