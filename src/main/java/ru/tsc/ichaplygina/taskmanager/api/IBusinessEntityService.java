package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessEntityService<E extends AbstractBusinessEntity> extends IService<E> {

    void add(String name, String description) throws AbstractException;

    void clear() throws AbstractException;

    E completeById(String id) throws AbstractException;

    E completeByIndex(int index) throws AbstractException;

    E completeByName(String name) throws AbstractException;

    List<E> findAll(Comparator<E> comparator) throws AbstractException;

    E findByIndex(int index) throws AbstractException;

    E findByName(String name) throws AbstractException;

    String getId(int index) throws AbstractException;

    String getId(String name) throws AbstractException;

    boolean isNotFoundById(String id) throws AbstractException;

    E removeByIndex(int index) throws AbstractException;

    E removeByName(String name) throws AbstractException;

    E startById(String id) throws AbstractException;

    E startByIndex(int index) throws AbstractException;

    E startByName(String name) throws AbstractException;

    E updateById(String id, String name, String description) throws AbstractException;

    E updateByIndex(int index, String name, String description) throws AbstractException;

    E updateStatusById(String id, Status status) throws AbstractException;

    E updateStatusByIndex(int index, Status status) throws AbstractException;

    E updateStatusByName(String name, Status status) throws AbstractException;

}
