package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.model.Project;

public class ProjectRepository extends AbstractBusinessEntityRepository<Project> implements IProjectRepository {

    @Override
    public void add(final String name, final String description, final String userId) {
        final Project project = new Project(name, description, userId);
        map.put(project.getId(), project);
    }

}
