package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityService;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public abstract class AbstractBusinessEntityService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessEntityService<E> {

    protected final IAuthService authService;

    protected final IBusinessEntityRepository<E> repository;

    public AbstractBusinessEntityService(final IBusinessEntityRepository<E> repository, final IAuthService authService) {
        super(repository);
        this.repository = repository;
        this.authService = authService;
    }

    @Override
    public void add(final String name, final String description) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        repository.add(name, description, authService.getCurrentUserId());
    }

    @Override
    public void clear() throws AbstractException {
        if (authService.isPrivilegedUser()) repository.clear();
        else repository.clearForUser(authService.getCurrentUserId());
    }

    @Override
    public E completeById(final String id) throws AbstractException {
        return updateStatusById(id, Status.COMPLETED);
    }

    @Override
    public E completeByIndex(final int index) throws AbstractException {
        return updateStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public E completeByName(final String name) throws AbstractException {
        return updateStatusByName(name, Status.COMPLETED);
    }

    @Override
    public List<E> findAll() throws AbstractException {
        if (authService.isPrivilegedUser()) return repository.findAll();
        return repository.findAllForUser(authService.getCurrentUserId());
    }

    @Override
    public List<E> findAll(final Comparator<E> comparator) throws AbstractException {
        if (authService.isPrivilegedUser()) return repository.findAll(comparator);
        return repository.findAllForUser(authService.getCurrentUserId(), comparator);
    }

    @Override
    public E findById(final String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (authService.isPrivilegedUser()) return repository.findById(id);
        return repository.findByIdForUser(authService.getCurrentUserId(), id);
    }

    @Override
    public E findByIndex(final int index) throws AbstractException {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        if (authService.isPrivilegedUser()) return repository.findByIndex(index);
        return repository.findByIndexForUser(authService.getCurrentUserId(), index);
    }

    @Override
    public E findByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        if (authService.isPrivilegedUser()) return repository.findByName(name);
        return repository.findByNameForUser(authService.getCurrentUserId(), name);
    }

    @Override
    public String getId(final int index) throws AbstractException {
        if (authService.isPrivilegedUser()) return repository.getId(index);
        return repository.getIdForUser(authService.getCurrentUserId(), index);
    }

    @Override
    public String getId(final String name) throws AbstractException {
        if (authService.isPrivilegedUser()) return repository.getId(name);
        return repository.getIdForUser(authService.getCurrentUserId(), name);
    }

    @Override
    public int getSize() throws AbstractException {
        if (authService.isPrivilegedUser()) return repository.getSize();
        else return repository.getSizeForUser(authService.getCurrentUserId());
    }

    @Override
    public boolean isEmpty() throws AbstractException {
        if (authService.isPrivilegedUser()) return repository.isEmpty();
        return repository.isEmptyForUser(authService.getCurrentUserId());
    }

    @Override
    public boolean isNotFoundById(final String id) throws AbstractException {
        if (authService.isPrivilegedUser()) return repository.isNotFoundById(id);
        return repository.isNotFoundByIdForUser(authService.getCurrentUserId(), id);
    }

    @Override
    public E removeById(final String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (authService.isPrivilegedUser()) return repository.removeById(id);
        return repository.removeByIdForUser(authService.getCurrentUserId(), id);
    }

    @Override
    public E removeByIndex(final int index) throws AbstractException {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        if (authService.isPrivilegedUser()) return repository.removeByIndex(index);
        return repository.removeByIndexForUser(authService.getCurrentUserId(), index);
    }

    @Override
    public E removeByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        if (authService.isPrivilegedUser()) return repository.removeByName(name);
        return repository.removeByNameForUser(authService.getCurrentUserId(), name);
    }

    @Override
    public E startById(final String id) throws AbstractException {
        return updateStatusById(id, Status.IN_PROGRESS);
    }

    @Override
    public E startByIndex(final int index) throws AbstractException {
        return updateStatusByIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public E startByName(final String name) throws AbstractException {
        return updateStatusByName(name, Status.IN_PROGRESS);
    }

    @Override
    public E updateById(final String id, final String name, final String description) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (isEmptyString(name)) throw new NameEmptyException();
        if (authService.isPrivilegedUser()) return repository.update(id, name, description);
        return repository.updateForUser(authService.getCurrentUserId(), id, name, description);
    }

    @Override
    public E updateByIndex(final int index, final String name, final String description) throws AbstractException {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        final String id = getId(index);
        return updateById(id, name, description);
    }

    @Override
    public E updateStatusById(final String id, final Status status) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (authService.isPrivilegedUser()) return repository.updateStatusById(id, status);
        return repository.updateStatusByIdForUser(authService.getCurrentUserId(), id, status);
    }

    @Override
    public E updateStatusByIndex(final int index, final Status status) throws AbstractException {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        if (authService.isPrivilegedUser()) return repository.updateStatusByIndex(index, status);
        return repository.updateStatusByIndexForUser(authService.getCurrentUserId(), index, status);
    }

    @Override
    public E updateStatusByName(final String name, final Status status) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        if (authService.isPrivilegedUser()) return repository.updateStatusByName(name, status);
        return repository.updateStatusByNameForUser(authService.getCurrentUserId(), name, status);
    }

}
