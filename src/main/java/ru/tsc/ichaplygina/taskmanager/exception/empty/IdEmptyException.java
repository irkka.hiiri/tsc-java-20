package ru.tsc.ichaplygina.taskmanager.exception.empty;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class IdEmptyException extends AbstractException {

    private static final String MESSAGE = "Error! Id is empty.";

    public IdEmptyException() {
        super(MESSAGE);
    }

}
