package ru.tsc.ichaplygina.taskmanager.command.task;

import com.sun.org.apache.xpath.internal.objects.XNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    private final static String NAME = "update task by id";

    private final static String DESCRIPTION = "update task by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        getAuthService().throwExceptionIfNotAuthorized();
        final String id = readLine(ID_INPUT);
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        if (getTaskService().updateById(id, name, description) == null) throw new TaskNotFoundException();
    }

}
