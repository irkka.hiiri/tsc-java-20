package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public abstract class AbstractBusinessEntityRepository<E extends AbstractBusinessEntity> extends AbstractModelRepository<E> implements IBusinessEntityRepository<E> {

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public void clearForUser(final String currentUserId) {
        for (E entity : findAllForUser(currentUserId)) remove(entity);
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        List<E> list = findAll();
        list.sort(comparator);
        return list;
    }

    @Override
    public List<E> findAllForUser(final String userId) {
        final List<E> list = new ArrayList<>();
        map.forEach((key, value) -> {
            if (value.getUserId().equals(userId)) list.add(value);
        });
        return list;
    }

    @Override
    public List<E> findAllForUser(final String userId, Comparator<E> comparator) {
        final List<E> list = findAllForUser(userId);
        list.sort(comparator);
        return list;
    }

    @Override
    public E findByIdForUser(final String userId, final String id) {
        for (E entity : findAllForUser(userId)) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findByIndex(final int index) {
        final List<E> list = findAll();
        return list.get(index);
    }

    @Override
    public E findByIndexForUser(final String userId, final int index) {
        final List<E> list = findAllForUser(userId);
        return list.get(index);
    }

    @Override
    public E findByName(final String name) {
        for (E entity : findAll()) {
            if (name.equals(entity.getName())) return entity;
        }
        return null;
    }

    @Override
    public E findByNameForUser(final String userId, final String name) {
        for (E entity : findAllForUser(userId)) {
            if (name.equals(entity.getName())) return entity;
        }
        return null;
    }

    @Override
    public String getId(final String name) {
        E entity = findByName(name);
        if (entity == null) return null;
        return entity.getId();
    }

    @Override
    public String getId(final int index) {
        E entity = findByIndex(index);
        if (entity == null) return null;
        return entity.getId();
    }

    @Override
    public String getIdForUser(final String userId, final int index) {
        E entity = findByIndexForUser(userId, index);
        if (entity == null) return null;
        return entity.getId();
    }

    @Override
    public String getIdForUser(final String userId, final String name) {
        E entity = findByNameForUser(userId, name);
        if (entity == null) return null;
        return entity.getId();
    }

    @Override
    public int getSizeForUser(final String userId) {
        final List<E> list = findAllForUser(userId);
        return list.size();
    }

    @Override
    public boolean isEmptyForUser(final String userId) {
        final List<E> list = findAllForUser(userId);
        return list.isEmpty();
    }

    @Override
    public boolean isNotFoundById(final String id) {
        return findById(id) == null;
    }

    @Override
    public boolean isNotFoundByIdForUser(final String userId, final String id) {
        return findByIdForUser(userId, id) == null;
    }

    @Override
    public E removeByIdForUser(final String userId, final String id) {
        final E entity = findByIdForUser(userId, id);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final int index) {
        final E entity = findByIndex(index);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Override
    public E removeByIndexForUser(final String userId, final int index) {
        final E entity = findByIndexForUser(userId, index);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Override
    public E removeByName(final String name) {
        final E entity = findByName(name);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Override
    public E removeByNameForUser(final String userId, final String name) {
        final E entity = findByNameForUser(userId, name);
        if (entity == null) return null;
        remove(entity);
        return entity;
    }

    @Override
    public E update(final String id, final String name, final String description) {
        final E entity = findById(id);
        if (entity == null) return null;
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateForUser(final String userId, final String id, final String name, final String description) {
        final E entity = findByIdForUser(userId, id);
        if (entity == null) return null;
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateStatusById(final String id, final Status status) {
        final E entity = findById(id);
        if (entity == null) return null;
        entity.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) entity.setDateStart(new Date());
        else if (status.equals(Status.COMPLETED)) entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E updateStatusByIdForUser(final String userId, final String id, final Status status) {
        final E entity = findByIdForUser(userId, id);
        if (entity == null) return null;
        entity.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) entity.setDateStart(new Date());
        else if (status.equals(Status.COMPLETED)) entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E updateStatusByIndex(final int index, final Status status) {
        final String id = getId(index);
        if (id == null) return null;
        return updateStatusById(id, status);
    }

    @Override
    public E updateStatusByIndexForUser(final String userId, final int index, final Status status) {
        final String id = getIdForUser(userId, index);
        if (id == null) return null;
        return updateStatusByIdForUser(userId, id, status);
    }

    @Override
    public E updateStatusByName(final String name, final Status status) {
        final String id = getId(name);
        if (id == null) return null;
        return updateStatusById(id, status);
    }

    @Override
    public E updateStatusByNameForUser(final String userId, final String name, final Status status) {
        final String id = getIdForUser(userId, name);
        if (id == null) return null;
        return updateStatusByIdForUser(userId, id, status);
    }

}
