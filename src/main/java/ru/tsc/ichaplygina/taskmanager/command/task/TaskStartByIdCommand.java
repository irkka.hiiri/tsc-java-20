package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    private final static String NAME = "start task by id";

    private final static String DESCRIPTION = "start task by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        getAuthService().throwExceptionIfNotAuthorized();
        final String id = readLine(ID_INPUT);
        if (getTaskService().startById(id) == null) throw new TaskNotFoundException();
    }

}
