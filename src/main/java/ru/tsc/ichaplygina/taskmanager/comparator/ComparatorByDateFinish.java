package ru.tsc.ichaplygina.taskmanager.comparator;

import ru.tsc.ichaplygina.taskmanager.api.entity.IHasDateFinish;

import java.util.Comparator;

public class ComparatorByDateFinish implements Comparator<IHasDateFinish> {

    private static final ComparatorByDateFinish INSTANCE = new ComparatorByDateFinish();

    private ComparatorByDateFinish() {
    }

    public static ComparatorByDateFinish getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasDateFinish o1, final IHasDateFinish o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getDateFinish() == null && o2.getDateFinish() == null) return 0;
        if (o1.getDateFinish() == null) return -1;
        if (o2.getDateFinish() == null) return 1;
        return o1.getDateFinish().compareTo(o2.getDateFinish());
    }

}
