package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public class TaskShowByIndexCommand extends AbstractTaskCommand {

    private final static String NAME = "show task by index";

    private final static String DESCRIPTION = "show task by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        getAuthService().throwExceptionIfNotAuthorized();
        final int index = readNumber(INDEX_INPUT);
        final Task task = getTaskService().findByIndex(index - 1);
        showTask(task);
    }

}
