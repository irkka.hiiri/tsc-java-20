package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readRole;

public class ChangeRoleCommand extends AbstractUserCommand {

    private static final String NAME = "change role";

    private static final String DESCRIPTION = "change user's role";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        getAuthService().throwExceptionIfNotPrivilegedUser();
        final String login = readLine(ENTER_LOGIN);
        final Role role = readRole(ENTER_ROLE);
        getUserService().setRole(login, role);
    }

}
