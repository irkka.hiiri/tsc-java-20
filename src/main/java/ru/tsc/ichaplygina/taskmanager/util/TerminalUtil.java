package ru.tsc.ichaplygina.taskmanager.util;

import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.enumerated.Sort;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.NEW_LINE;
import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.SORT_INPUT;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public final class TerminalUtil {

    private final static Scanner SCANNER = new Scanner(System.in);

    private TerminalUtil() {
    }

    public static void printLinesWithEmptyLine(final Object... lines) {
        System.out.println();
        for (Object line : lines) System.out.println(line);
        System.out.println();
    }

    public static <T> void printList(final List<T> list) {
        if (list == null) return;
        System.out.println();
        for (Object item : list) {
            System.out.println(item);
        }
        System.out.println();
    }

    public static <T> void printListWithIndexes(final List<T> list) {
        if (list == null) return;
        System.out.println();
        int index = 1;
        for (Object item : list) {
            System.out.println(index + ". " + item);
            index++;
        }
        System.out.println();
    }

    public static Comparator readComparator() {
        final String sort = readLine(SORT_INPUT + " " + Arrays.toString(Sort.values()) + NEW_LINE);
        if (isEmptyString(sort)) return Sort.CREATED.getComparator();
        try {
            return Sort.valueOf(sort.toUpperCase()).getComparator();
        } catch (final IllegalArgumentException e) {
            return Sort.CREATED.getComparator();
        }
    }

    public static String readLine() {
        return SCANNER.nextLine().trim();
    }

    public static String readLine(final String output) {
        System.out.print(output);
        return readLine();
    }

    public static int readNumber() {
        try {
            return Integer.parseInt(readLine());
        } catch (final NumberFormatException e) {
            return -1;
        }
    }

    public static int readNumber(final String output) {
        System.out.print(output);
        return readNumber();
    }

    public static Role readRole(final String output) {
        final String role = readLine(output + " " + Arrays.toString(Role.values()) + NEW_LINE);
        if (isEmptyString(role)) return Role.USER;
        try {
            return Role.valueOf(role.toUpperCase());
        } catch (final IllegalArgumentException e) {
            return Role.USER;
        }
    }

}
