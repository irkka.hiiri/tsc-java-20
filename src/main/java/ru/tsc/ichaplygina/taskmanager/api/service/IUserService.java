package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.api.IService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.User;

public interface IUserService extends IService<User> {

    void add(String login, String password, String email, Role role,
             String firstName, String middleName, String lastName) throws AbstractException;

    User findByLogin(String login) throws AbstractException;

    User removeByLogin(String login) throws AbstractException;

    void setPassword(String login, String password) throws AbstractException;

    void setRole(String login, Role role) throws AbstractException;

    User updateById(String id, String login, String password, String email,
                    Role role, String firstName, String middleName, String lastName) throws AbstractException;

    User updateByLogin(String login, String password, String email,
                       Role role, String firstName, String middleName, String lastName) throws AbstractException;

}
