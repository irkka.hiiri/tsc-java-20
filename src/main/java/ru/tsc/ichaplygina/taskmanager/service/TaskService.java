package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractBusinessEntityService<Task> implements ITaskService {

    private final ITaskRepository repository;

    public TaskService(ITaskRepository repository, IAuthService authService) {
        super(repository, authService);
        this.repository = repository;
    }

    @Override
    public Task addTaskToProject(final String taskId, final String projectId) throws AbstractException {
        if (authService.isPrivilegedUser()) return repository.addTaskToProject(taskId, projectId);
        return repository.addTaskToProjectForUser(authService.getCurrentUserId(), taskId, projectId);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId, final Comparator<Task> comparator) throws AbstractException {
        if (authService.isPrivilegedUser()) return repository.findAllByProjectId(projectId, comparator);
        return repository.findAllByProjectIdForUser(authService.getCurrentUserId(), projectId, comparator);
    }

    @Override
    public void removeAllByProjectId(final String projectId) {
        repository.removeAllByProjectId(projectId);
    }

    @Override
    public Task removeTaskFromProject(final String taskId, final String projectId) throws AbstractException {
        if (authService.isPrivilegedUser()) return repository.removeTaskFromProject(taskId, projectId);
        return repository.removeTaskFromProjectForUser(authService.getCurrentUserId(), taskId, projectId);
    }

}
