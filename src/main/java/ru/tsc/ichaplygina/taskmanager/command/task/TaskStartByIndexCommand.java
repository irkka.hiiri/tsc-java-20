package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public class TaskStartByIndexCommand extends AbstractTaskCommand {

    private final static String NAME = "start task by index";

    private final static String DESCRIPTION = "start task by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        getAuthService().throwExceptionIfNotAuthorized();
        final int index = readNumber(INDEX_INPUT);
        if (getTaskService().startByIndex(index - 1) == null) throw new TaskNotFoundException();
    }

}
