package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.User;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class UserShowByIdCommand extends AbstractUserCommand {

    private static final String NAME = "show user by id";

    private static final String DESCRIPTION = "show user by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        getAuthService().throwExceptionIfNotPrivilegedUser();
        final String id = readLine(ID_INPUT);
        final User user = getUserService().findById(id);
        showUser(user);
    }

}
