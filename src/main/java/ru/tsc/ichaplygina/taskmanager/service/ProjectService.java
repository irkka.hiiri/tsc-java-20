package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.model.Project;

public class ProjectService extends AbstractBusinessEntityService<Project> implements IProjectService {

    public ProjectService(IProjectRepository repository, IAuthService authService) {
        super(repository, authService);
    }

}
