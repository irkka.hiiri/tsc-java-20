package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class TaskCreateCommand extends AbstractTaskCommand {

    private final static String NAME = "create task";

    private final static String DESCRIPTION = "create a new task";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        getAuthService().throwExceptionIfNotAuthorized();
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        getTaskService().add(name, description);
        printLinesWithEmptyLine("Done. Type <list tasks> to view all tasks.");
    }

}
