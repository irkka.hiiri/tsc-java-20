package ru.tsc.ichaplygina.taskmanager.api.repository;

import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityRepository;
import ru.tsc.ichaplygina.taskmanager.model.Project;

public interface IProjectRepository extends IBusinessEntityRepository<Project> {

}
