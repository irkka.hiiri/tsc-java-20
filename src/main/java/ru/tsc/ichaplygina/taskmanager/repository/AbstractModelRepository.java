package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.IRepository;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractModelRepository<E extends AbstractModel> implements IRepository<E> {

    protected final Map<String, E> map = new LinkedHashMap<>();

    @Override
    public void add(final E entity) {
        map.put(entity.getId(), entity);
    }

    @Override
    public List<E> findAll() {
        return new ArrayList<>(map.values());
    }

    @Override
    public E findById(final String id) {
        return map.get(id);
    }

    @Override
    public int getSize() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public void remove(final E entity) {
        map.remove(entity.getId());
    }

    @Override
    public E removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        this.remove(entity);
        return entity;
    }

}
