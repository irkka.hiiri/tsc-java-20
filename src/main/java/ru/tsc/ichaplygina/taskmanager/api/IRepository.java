package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.AbstractModel;

import java.util.List;

public interface IRepository<E extends AbstractModel> {

    void add(E entity);

    List<E> findAll();

    E findById(String id);

    int getSize();

    boolean isEmpty();

    void remove(E entity);

    E removeById(String id);

}
