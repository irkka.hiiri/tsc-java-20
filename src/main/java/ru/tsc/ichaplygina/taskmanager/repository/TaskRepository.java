package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractBusinessEntityRepository<Task> implements ITaskRepository {

    @Override
    public void add(final String name, final String description, final String userId) {
        final Task task = new Task(name, description, userId);
        map.put(task.getId(), task);
    }

    @Override
    public Task addTaskToProject(final String taskId, final String projectId) {
        final Task task = findById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task addTaskToProjectForUser(final String userId, final String taskId, final String projectId) {
        final Task task = findByIdForUser(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        List<Task> list = new ArrayList<>();
        for (Task task : super.findAll()) {
            if (projectId.equals(task.getProjectId())) list.add(task);
        }
        return list;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId, Comparator<Task> comparator) {
        final List<Task> list = findAllByProjectId(projectId);
        list.sort(comparator);
        return list;
    }

    @Override
    public List<Task> findAllByProjectIdForUser(final String userId, final String projectId) {
        List<Task> list = new ArrayList<>();
        for (Task task : findAllForUser(userId)) {
            if (projectId.equals(task.getProjectId())) list.add(task);
        }
        return list;
    }

    @Override
    public List<Task> findAllByProjectIdForUser(final String userId, final String projectId, Comparator<Task> comparator) {
        final List<Task> list = findAllByProjectIdForUser(userId, projectId);
        list.sort(comparator);
        return list;
    }

    @Override
    public boolean isNotFoundTaskInProject(final String taskId, final String projectId) {
        return (!findAllByProjectId(projectId).contains(findById(taskId)));
    }

    @Override
    public void removeAllByProjectId(final String projectId) {
        findAllByProjectId(projectId).forEach(task -> map.remove(task.getId()));
    }

    @Override
    public Task removeTaskFromProject(final String taskId, final String projectId) {
        final Task task = findById(taskId);
        if (task == null) return null;
        if (!projectId.equals(task.getProjectId())) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public Task removeTaskFromProjectForUser(final String userId, final String taskId, final String projectId) {
        final Task task = findByIdForUser(userId, taskId);
        if (task == null) return null;
        if (!projectId.equals(task.getProjectId())) return null;
        task.setProjectId(null);
        return task;
    }

}
