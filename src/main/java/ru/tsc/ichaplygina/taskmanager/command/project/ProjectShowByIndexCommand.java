package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectShowByIndexCommand extends AbstractProjectCommand {

    private final static String NAME = "show project by index";

    private final static String DESCRIPTION = "show project by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        getAuthService().throwExceptionIfNotAuthorized();
        final int index = readNumber(INDEX_INPUT);
        final Project project = getProjectService().findByIndex(index - 1);
        showProject(project);
    }

}
