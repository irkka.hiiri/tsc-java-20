package ru.tsc.ichaplygina.taskmanager.bootstrap;

import ru.tsc.ichaplygina.taskmanager.api.ServiceLocator;
import ru.tsc.ichaplygina.taskmanager.api.repository.*;
import ru.tsc.ichaplygina.taskmanager.api.service.*;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.command.project.*;
import ru.tsc.ichaplygina.taskmanager.command.project.ProjectUpdateByIndexCommand;
import ru.tsc.ichaplygina.taskmanager.command.system.*;
import ru.tsc.ichaplygina.taskmanager.command.task.*;
import ru.tsc.ichaplygina.taskmanager.command.user.*;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.CommandDescriptionEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.CommandNameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.CommandNotFoundException;
import ru.tsc.ichaplygina.taskmanager.repository.*;
import ru.tsc.ichaplygina.taskmanager.service.*;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public final class Bootstrap implements ServiceLocator {

    private static final boolean CONSOLE_LOG_ENABLED = true;

    private final ILogService logService = new LogService(CONSOLE_LOG_ENABLED);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthRepository authRepository = new AuthRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IAuthService authService = new AuthService(authRepository, userService);

    private final ITaskService taskService = new TaskService(taskRepository, authService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository, authService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskService, projectService, authService);

    public void executeCommand(final String commandName) throws AbstractException {
        if (isEmptyString(commandName)) return;
        final AbstractCommand command = commandService.getCommands().get(commandName);
        if (command == null) throw new CommandNotFoundException(commandName);
        command.execute();
    }

    public void executeCommand(final String[] params) throws AbstractException {
        if (params == null || params.length == 0) return;
        final AbstractCommand command = commandService.getArguments().get(params[0]);
        if (command == null) throw new CommandNotFoundException(params[0]);
        command.execute();
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    public void initCommands() {
        initTaskCommands();
        initProjectCommands();
        initUserCommands();
        initSystemCommands();
    }

    private void initData() {
        try {
            userService.add("root", "toor", "root@domain", Role.ADMIN, "Root", "Root", "Root");
            userService.add("vasya", "123", "vasya@domain", Role.USER, "Vassily", "Ivanovich", "Pupkin");
            userService.add("vova", "123", "vova@domain", Role.USER, "Vova", "V.", "P.");
            authService.login("root", "toor");
            taskService.add("en", "Sample Task");
            taskService.add("to", "Sample Task");
            taskService.add("tre", "Sample Task");
            taskService.add("fire", "Sample Task");
            taskService.add("fem", "Sample Task");
            projectService.add("Yksi", "Sample Project");
            projectService.add("Kaksi", "Sample Project");
            projectService.add("Kolme", "Sample Project");
            projectService.add("Nelja", "Sample Project");
            projectService.add("Viisi", "Sample Project");
            authService.logout();
            authService.login("vasya", "123");
            taskService.add("vasya task 1", "Sample Task");
            taskService.add("vasya task 2", "Sample Task");
            projectService.add("Vasya project 1", "Sample Project");
            projectService.add("Vasya project 2", "Sample Project");
            authService.logout();
            authService.login("vova", "123");
            taskService.add("vova task 1", "Sample Task");
            taskService.add("vova task 2", "Sample Task");
            projectService.add("vova project 1", "Sample Project");
            projectService.add("vova project 2", "Sample Project");
            authService.logout();
        } catch (final AbstractException e) {
            logService.error(e);
        }
    }

    public void initProjectCommands() {
        registerCommand(new ProjectListCommand());
        registerCommand(new ProjectCreateCommand());
        registerCommand(new ProjectClearCommand());
        registerCommand(new ProjectShowByIdCommand());
        registerCommand(new ProjectShowByIndexCommand());
        registerCommand(new ProjectShowByNameCommand());
        registerCommand(new ProjectUpdateByIdCommand());
        registerCommand(new ProjectUpdateByIndexCommand());
        registerCommand(new ProjectRemoveByIdCommand());
        registerCommand(new ProjectRemoveByIndexCommand());
        registerCommand(new ProjectRemoveByNameCommand());
        registerCommand(new ProjectStartByIdCommand());
        registerCommand(new ProjectStartByIndexCommand());
        registerCommand(new ProjectStartByNameCommand());
        registerCommand(new ProjectCompleteByIdCommand());
        registerCommand(new ProjectCompleteByIndexCommand());
        registerCommand(new ProjectCompleteByNameCommand());
    }

    public void initSystemCommands() {
        registerCommand(new InfoCommand());
        registerCommand(new AboutCommand());
        registerCommand(new VersionCommand());
        registerCommand(new HelpCommand());
        registerCommand(new ListCommandsCommand());
        registerCommand(new ListArgumentsCommand());
        registerCommand(new ExitCommand());
    }

    public void initTaskCommands() {
        registerCommand(new TaskListCommand());
        registerCommand(new TaskCreateCommand());
        registerCommand(new TaskClearCommand());
        registerCommand(new TaskShowByIdCommand());
        registerCommand(new TaskShowByIndexCommand());
        registerCommand(new TaskShowByNameCommand());
        registerCommand(new TaskUpdateByIdCommand());
        registerCommand(new TaskUpdateByIndexCommand());
        registerCommand(new TaskRemoveByIdCommand());
        registerCommand(new TaskRemoveByIndexCommand());
        registerCommand(new TaskRemoveByNameCommand());
        registerCommand(new TaskStartByIdCommand());
        registerCommand(new TaskStartByIndexCommand());
        registerCommand(new TaskStartByNameCommand());
        registerCommand(new TaskCompleteByIdCommand());
        registerCommand(new TaskCompleteByIndexCommand());
        registerCommand(new TaskCompleteByNameCommand());
        registerCommand(new TaskListByProjectCommand());
        registerCommand(new TaskAddToProjectCommand());
        registerCommand(new TaskRemoveFromProjectCommand());
    }

    public void initUserCommands() {
        registerCommand(new LoginCommand());
        registerCommand(new LogoutCommand());
        registerCommand(new UserListCommand());
        registerCommand(new UserAddCommand());
        registerCommand(new UserShowByIdCommand());
        registerCommand(new UserShowByLoginCommand());
        registerCommand(new UserUpdateByIdCommand());
        registerCommand(new UserUpdateByLoginCommand());
        registerCommand(new ChangePasswordCommand());
        registerCommand(new ChangeRoleCommand());
        registerCommand(new UserRemoveByIdCommand());
        registerCommand(new UserRemoveByLoginCommand());
        registerCommand(new WhoAmICommand());
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public void processInput() {
        logService.info("Program run in command-line mode.");
        initData();
        showWelcome();
        String command = readCommand();
        while (true) {
            try {
                if (isEmptyString(command)) {
                    command = readCommand();
                    continue;
                }
                logService.command("Executing command: " + command);
                executeCommand(command);
                System.out.println(APP_COMMAND_SUCCESS);
            } catch (final AbstractException e) {
                logService.error(e);
                System.out.println(APP_COMMAND_ERROR);
            }
            command = readCommand();
        }
    }

    public String readCommand() {
        return TerminalUtil.readLine(COMMAND_PROMPT);
    }

    public void registerCommand(final AbstractCommand command) {
        try {
            final String terminalCommand = command.getCommand();
            final String commandDescription = command.getDescription();
            final String commandArgument = command.getArgument();
            if (isEmptyString(terminalCommand)) throw new CommandNameEmptyException();
            if (isEmptyString(commandDescription)) throw new CommandDescriptionEmptyException();
            command.setServiceLocator(this);
            commandService.getCommands().put(terminalCommand, command);
            if (isEmptyString(commandArgument)) return;
            commandService.getArguments().put(commandArgument, command);
        } catch (AbstractException e) {
            logService.error(e);
        }
    }

    public void run(final String... args) {
        initCommands();
        if (args == null || args.length == 0) processInput();
        else
            try {
                logService.info("Program run in arguments mode.");
                executeCommand(args);
            } catch (final AbstractException e) {
                logService.error(e);
            }
    }

    private void showWelcome() {
        printLinesWithEmptyLine(APP_WELCOME_TEXT);
    }
}
