package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.EmailEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWIthEmailException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithLoginException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.model.User;

import static ru.tsc.ichaplygina.taskmanager.util.HashUtil.salt;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public void add(final String login, final String password, final String email, final Role role,
                    final String firstName, final String middleName, final String lastName) throws AbstractException {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        if (userRepository.isFoundByLogin(login)) throw new UserExistsWithLoginException(login);
        if (userRepository.isFoundByEmail(email)) throw new UserExistsWIthEmailException(email);
        userRepository.add(new User(login, salt(password), email, firstName, middleName, lastName, role));
    }

    @Override
    public User findByLogin(final String login) throws AbstractException {
        final String id = findIdByLogin(login);
        if (id == null) return null;
        return findById(id);
    }

    private String findIdByLogin(final String login) throws AbstractException {
        if (isEmptyString(login)) throw new LoginEmptyException();
        return userRepository.findIdByLogin(login);
    }

    @Override
    public User removeByLogin(final String login) throws AbstractException {
        final String id = findIdByLogin(login);
        if (id == null) return null;
        return removeById(id);
    }

    @Override
    public void setPassword(final String login, final String password) throws AbstractException {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(salt(password));
    }

    @Override
    public void setRole(final String login, final Role role) throws AbstractException {
        if (isEmptyString(login)) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setRole(role);
    }

    @Override
    public User updateById(final String id, final String login, final String password, final String email,
                           final Role role, final String firstName, final String middleName, final String lastName) throws AbstractException {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        if (userRepository.isFoundByLogin(login) && !id.equals(userRepository.findByLogin(login).getId()))
            throw new UserExistsWithLoginException(login);
        if (userRepository.isFoundByEmail(email) && !id.equals(userRepository.findByEmail(email).getId()))
            throw new UserExistsWIthEmailException(email);
        return userRepository.update(id, login, password, email, role, firstName, middleName, lastName);
    }

    @Override
    public User updateByLogin(final String login, final String password, final String email,
                              final Role role, final String firstName, final String middleName, final String lastName) throws AbstractException {
        final String id = userRepository.findIdByLogin(login);
        if (id == null) return null;
        return updateById(id, login, password, email, role, firstName, middleName, lastName);
    }

}
