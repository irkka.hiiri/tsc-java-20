package ru.tsc.ichaplygina.taskmanager.api;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessEntityRepository<E extends AbstractBusinessEntity> extends IRepository<E> {

    void add(String userId, String name, String description);

    void clear() throws AbstractException;

    void clearForUser(String currentUserId);

    List<E> findAll(Comparator<E> comparator);

    List<E> findAllForUser(String userId);

    List<E> findAllForUser(String userId, Comparator<E> comparator);

    E findByIdForUser(String userId, String id);

    E findByIndex(int index);

    E findByIndexForUser(String userId, int index);

    E findByName(String name);

    E findByNameForUser(String userId, String name);

    String getId(String name);

    String getId(int index);

    String getIdForUser(String userId, int index);

    String getIdForUser(String userId, String name);

    int getSizeForUser(String userId);

    boolean isEmptyForUser(String userId);

    boolean isNotFoundById(String id);

    boolean isNotFoundByIdForUser(String userId, String id);

    E removeByIdForUser(String userId, String id);

    E removeByIndex(int index);

    E removeByIndexForUser(String userId, int index);

    E removeByName(String name);

    E removeByNameForUser(String userId, String name);

    E update(String id, String name, String description);

    E updateForUser(String userId, String id, String name, String description);

    E updateStatusById(String id, Status status);

    E updateStatusByIdForUser(String userId, String id, Status status);

    E updateStatusByIndex(int index, Status status);

    E updateStatusByIndexForUser(String userId, int index, Status status);

    E updateStatusByName(String name, Status status);

    E updateStatusByNameForUser(String userId, String name, Status status);

}
