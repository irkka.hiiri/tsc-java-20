package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectTaskService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectService projectService;

    private final ITaskService taskService;

    public ProjectTaskService(final ITaskService taskService, final IProjectService projectService, final IAuthService authService) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Override
    public Task addTaskToProject(final String projectId, final String taskId) throws AbstractException {
        if (isEmptyString(projectId) || isEmptyString(taskId)) throw new IdEmptyException();
        if (projectService.isNotFoundById(projectId)) throw new ProjectNotFoundException();
        return (taskService.addTaskToProject(taskId, projectId));
    }

    @Override
    public void clearProjects() throws AbstractException {
        for (final Project project : projectService.findAll())
            taskService.removeAllByProjectId(project.getId());
        projectService.clear();
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId, final Comparator<Task> taskComparator)
            throws AbstractException {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        if (projectService.isNotFoundById(projectId)) throw new ProjectNotFoundException();
        return taskService.findAllByProjectId(projectId, taskComparator);
    }

    @Override
    public Project removeProjectById(final String projectId) throws AbstractException {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        if (projectService.findById(projectId) == null) throw new ProjectNotFoundException();
        taskService.removeAllByProjectId(projectId);
        return projectService.removeById(projectId);
    }

    @Override
    public Project removeProjectByIndex(final int index) throws AbstractException {
        if (isInvalidListIndex(index, projectService.getSize())) throw new IndexIncorrectException(index + 1);
        final String id = projectService.getId(index);
        return removeProjectById(id);
    }

    @Override
    public Project removeProjectByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final String id = projectService.getId(name);
        if (isEmptyString(id)) throw new ProjectNotFoundException();
        return removeProjectById(id);
    }

    @Override
    public Task removeTaskFromProject(final String projectId, final String taskId) throws AbstractException {
        if (isEmptyString(projectId) || isEmptyString(taskId)) throw new IdEmptyException();
        if (projectService.isNotFoundById(projectId)) throw new ProjectNotFoundException();
        return (taskService.removeTaskFromProject(taskId, projectId));
    }

}
