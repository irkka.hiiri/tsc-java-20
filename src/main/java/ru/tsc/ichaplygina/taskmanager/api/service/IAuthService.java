package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public interface IAuthService {

    String getCurrentUserId();

    void setCurrentUserId(String currentUserId) throws AbstractException;

    String getCurrentUserLogin() throws AbstractException;

    boolean isNoUserLoggedIn();

    boolean isPrivilegedUser() throws AbstractException;

    void login(String login, String password) throws AbstractException;

    void logout() throws AbstractException;

    void throwExceptionIfNotAuthorized() throws AbstractException;

    void throwExceptionIfNotPrivilegedUser() throws AbstractException;
}
