package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectTaskService {

    Task addTaskToProject(String projectId, String taskId) throws AbstractException;

    void clearProjects() throws AbstractException;

    List<Task> findAllTasksByProjectId(String projectId, Comparator<Task> taskComparator)
            throws AbstractException;

    Project removeProjectById(String projectId) throws AbstractException;

    Project removeProjectByIndex(int projectIndex) throws AbstractException;

    Project removeProjectByName(String name) throws AbstractException;

    Task removeTaskFromProject(String projectId, String taskId) throws AbstractException;

}
