package ru.tsc.ichaplygina.taskmanager.constant;

public class StringConst {

    public static final String NEW_LINE = "\n";

    public static final String EMPTY = "";

    public static final String DELIMITER = " : ";

    public static final String PLACEHOLDER = "<empty>";

    public static final String COMMAND_PROMPT = "> ";

    public static final String SORT_INPUT = "Sort output by: (optional) ";

    public static final String APP_COMMAND_SUCCESS = "Command completed successfully.";

    public static final String APP_COMMAND_ERROR = "An error has occurred.";

    public static final String APP_VERSION = "0.20.0";

    public static final String APP_WELCOME_TEXT = "*** WELCOME TO THE ULTIMATE TASK MANAGER ***" +
            NEW_LINE +
            NEW_LINE +
            "Enter <help> to show available commands." +
            NEW_LINE +
            "Enter <exit> to quit." +
            NEW_LINE +
            "Enter command: ";

    public static final String APP_HELP_HINT_TEXT = "Run with command-line arguments (as listed in [brackets])." +
            NEW_LINE +
            "Run with no arguments to enter command-line mode." +
            NEW_LINE +
            NEW_LINE +
            "Available commands and arguments:";

    public static final String APP_ABOUT = "Developed by:" +
            NEW_LINE +
            "Irina Chaplygina," +
            NEW_LINE +
            "Technoserv Consulting," +
            NEW_LINE +
            "ichaplygina@tsconsulting.com";

    public static final String SYSINFO_NO_LIMIT_TEXT = "No limit";

    public static final String SYSINFO_PROCESSORS = "Available processors: ";

    public static final String SYSINFO_FREE_MEMORY = "Free memory: ";

    public static final String SYSINFO_MAX_MEMORY = "Maximum memory: ";

    public static final String SYSINFO_TOTAL_MEMORY = "Total memory available to JVM: ";

    public static final String SYSINFO_USED_MEMORY = "Memory used by JVM: ";

}
