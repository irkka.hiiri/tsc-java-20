package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;

public class TaskClearCommand extends AbstractTaskCommand {

    private final static String NAME = "clear tasks";

    private final static String DESCRIPTION = "delete all tasks";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        getAuthService().throwExceptionIfNotAuthorized();
        final int count = getTaskService().getSize();
        getTaskService().clear();
        printLinesWithEmptyLine(count + " tasks removed.");
    }

}
