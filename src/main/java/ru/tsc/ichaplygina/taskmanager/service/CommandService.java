package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.ICommandRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.ICommandService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Map<String, AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commandRepository.getCommands().values());
    }

    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

}
