package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.model.User;

public class UserRepository extends AbstractModelRepository<User> implements IUserRepository {

    @Override
    public User findByEmail(final String email) {
        for (User user : findAll()) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User findById(final String id) {
        return map.get(id);
    }

    @Override
    public User findByLogin(final String login) {
        for (User user : findAll()) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public String findIdByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return user.getId();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean isFoundByEmail(final String email) {
        return (findByEmail(email)) != null;
    }

    @Override
    public boolean isFoundByLogin(final String login) {
        return (findByLogin(login)) != null;
    }

    @Override
    public void remove(final User user) {
        map.remove(user.getId());
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (user != null) remove(user);
        return user;
    }

    @Override
    public void removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user != null) remove(user);
    }

    @Override
    public User update(final String id, final String login, final String password, final String email,
                       final Role role, final String firstName, final String middleName, final String lastName) {
        final User user = findById(id);
        if (user == null) return null;
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

}
