package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class LoginCommand extends AbstractUserCommand {

    private static final String NAME = "login";

    private static final String DESCRIPTION = "login into the system";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        final String login = readLine(ENTER_LOGIN);
        final String password = readLine(ENTER_PASSWORD);
        getAuthService().login(login, password);
    }

}
