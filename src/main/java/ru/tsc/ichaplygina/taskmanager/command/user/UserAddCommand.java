package ru.tsc.ichaplygina.taskmanager.command.user;

import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readRole;

public class UserAddCommand extends AbstractUserCommand {

    private static final String NAME = "add user";

    private static final String DESCRIPTION = "add a new user";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        getAuthService().throwExceptionIfNotPrivilegedUser();
        final String login = readLine(ENTER_LOGIN);
        final String password = readLine(ENTER_PASSWORD);
        final String email = readLine("Enter email: ");
        final Role role = readRole(ENTER_ROLE);
        final String firstName = readLine("Enter first name: ");
        final String middleName = readLine("Enter middle name: ");
        final String lastName = readLine("Enter last name: ");
        getUserService().add(login, password, email, role, firstName, middleName, lastName);
    }

}
