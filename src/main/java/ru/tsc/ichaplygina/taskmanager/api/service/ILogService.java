package ru.tsc.ichaplygina.taskmanager.api.service;

public interface ILogService {

    void command(String message);

    void error(Exception e);

    void info(String message);

}
